package sbu.cs;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ServerClass {
    private final int portNumber;
    private ServerSocket ss;
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    PrintStream ps;

    public ServerClass(int portNumber) throws IOException {
        this.portNumber = portNumber;
        ps = System.out;

        ps.println("trying to establish server on "+ portNumber);
        ss = new ServerSocket(portNumber);
        ps.println("server established");
    }

    public void waitForClient () throws IOException, InterruptedException {
        ps.println("waiting for client ...");

        socket = ss.accept();
        ps.println("client connected");
        in = new DataInputStream(socket.getInputStream());
        out = new DataOutputStream(socket.getOutputStream());
    }

    public void create_Copy (String folder) throws IOException {
        int transferSize = 1048576;
        Path path = Paths.get(folder);
        Files.createDirectories(path);
        long fileSize = in.readLong();
        long readData = 0;

        String fileName = in.readUTF();

        byte[] buffer = new byte[transferSize];
        File file = new File(folder + "/" + fileName);
        FileOutputStream fos = new FileOutputStream(file, true);

        int len;
        while (true) {
            len = in.read(buffer);
            readData = readData + (long) len;

            byte[] tmp = new byte[len];
            System.arraycopy(buffer, 0, tmp, 0, len);
            fos.write(tmp);
            fos.flush();

            if (readData == fileSize) {
                    break;
            }
        }
        fos.close();
    }

    public void portClose () throws IOException{
        ss.close();
    }

    public void close () throws IOException{
        socket.close();
        ss.close();
    }
}

package sbu.cs;


import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class ClientClass {
    private final String address;
    private final int portNumber;
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private PrintStream ps;

    public ClientClass (String address, int portNumber) throws UnknownHostException, IOException {
        this.address = address;
        this.portNumber = portNumber;
        ps = System.out;

        System.out.println("trying to connect ... ");
        socket = new Socket(address, portNumber);
        in = new DataInputStream(socket.getInputStream());
        out = new DataOutputStream(socket.getOutputStream());
        System.out.println("connected.");
    }

    public void fileSend (String filePath) throws FileNotFoundException, IOException {
        int transferSize = 1048576;
        File file = new File(filePath);
        Path path = Paths.get(filePath);
        out.writeLong(Files.size(path));
        out.writeUTF(filePath);
        out.flush();
        FileInputStream fis = new FileInputStream(file);

        byte[] buffer = new byte[transferSize];

        int len = fis.available();
        while (len > 0) {
            len = fis.read(buffer);
            if (len < transferSize) {
                byte[] tmp = new byte[len];
                System.arraycopy(buffer, 0, tmp, 0, len);
                out.write(tmp);
                out.flush();
                break;
            }
            out.write(buffer);
            out.flush();
            if (fis.available() == 0) {
                break;
            }
        }
        fis.close();

    }

    public void close () throws IOException {
        socket.close();
    }



}
